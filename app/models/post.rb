class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy 
  validates_presence_of :title
  validates_presence_of :body
   #so when we delete a post it deletes all the comments associated with it
  # comments is plural because of the has_many
end
